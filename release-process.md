# Javafuzz release process

## Versioning

Javafuzz uses [Semantic Versioning](https://semver.org/) to communicate the kind of changes included in a release. 

In order to update the version, the version number needs to manually updated be in `pom.xml` `core/pom.xml` and `javafuzz-maven-plugin/pom.xml` and the `README.md`


## Publishing

This code is published to the Package registry that is associated with this Project.

https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz/-/packages

There is a manual deploy job in the `.gitlab-ci.yml` that when run will publish to the Maven package registry. 


## Historical releases

Historical releases associated with https://github.com/fuzzitdev/javafuzz were published to Sonatype. Those packages are no longer updated and users should update their references to the the GitLab package location outlined as outlined in the [README.md](../README.md) 

