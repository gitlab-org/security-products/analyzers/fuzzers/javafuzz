package dev.fuzzit.javafuzz.core;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class CorpusTest {

    @Test
    public void shouldAcceptInputLongerThan127Bytes() {
        byte[] input = new byte[4096];
        Arrays.fill(input, (byte) ThreadLocalRandom.current().nextInt(48, 58));

        int i = 0;
        while (i < 10000) {
            byte[] mutatedBuffer = new com.gitlab.javafuzz.core.Corpus(null).mutate(input);

            for (int j = 0; j < mutatedBuffer.length; j++) {
                double digit = mutatedBuffer[j];
                assertTrue(digit >= 48 && digit <= 57);
            }
            i++;
        }
    }

    @Test
    public void shouldLimitTheResponseLengthTo4096() {
        byte[] input = new byte[9999];
        Arrays.fill(input, (byte) ThreadLocalRandom.current().nextInt(48, 58));

        byte[] mutatedBuffer = new com.gitlab.javafuzz.core.Corpus(null).mutate(input);
        assertEquals(mutatedBuffer.length, 4096);
    }

    @Test
    public void shouldNotAddDuplicateItemsToCorpus() throws NoSuchAlgorithmException {
        com.gitlab.javafuzz.core.Corpus corpus = new com.gitlab.javafuzz.core.Corpus(null);
        byte[] previousCorpus = {9, 2, 3, 4, 5};
        corpus.putBuffer(previousCorpus);

        int previousCorpusLength = corpus.getLength();

        corpus.putBuffer(previousCorpus);

        int updatedCorpusLength = corpus.getLength();

        assertEquals(previousCorpusLength, updatedCorpusLength);
    }

    @Test
    public void shouldAddNewItemsToCorpus() throws NoSuchAlgorithmException {
        com.gitlab.javafuzz.core.Corpus corpus = new com.gitlab.javafuzz.core.Corpus(null);
        byte[] firstCorpus = {1, 2, 3};
        corpus.putBuffer(firstCorpus);

        int previousCorpusLength = corpus.getLength();

        byte[] secondCorpus = {4, 5, 6};
        corpus.putBuffer(secondCorpus);

        int updatedCorpusLength = corpus.getLength();

        assertEquals(1, previousCorpusLength);
        assertEquals(2, updatedCorpusLength);

        assertEquals(corpus.getInputs().get(0), firstCorpus);
        assertEquals(corpus.getInputs().get(1), secondCorpus);

    }
}
