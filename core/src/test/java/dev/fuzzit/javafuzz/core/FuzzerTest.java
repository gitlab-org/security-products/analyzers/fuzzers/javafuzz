package dev.fuzzit.javafuzz.core;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class FuzzerTest {

    @Test
    public void shouldNotDuplicateTheCorpusWhenExistingCorpus() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, NoSuchAlgorithmException {
        com.gitlab.javafuzz.core.AbstractFuzzTarget fuzzTarget = mock(com.gitlab.javafuzz.core.AbstractFuzzTarget.class);
        String dirs = Paths.get("..")+"/resources/corpus";

        com.gitlab.javafuzz.core.Fuzzer fuzzer = new com.gitlab.javafuzz.core.Fuzzer(fuzzTarget, dirs);

        int previousCorpusLength = fuzzer.getCorpusLength();

        doNothing().doThrow(new RuntimeException("Test")).when(fuzzTarget).fuzz(isA(byte[].class));
        fuzzer.start();

        int updatedCorpusLength = fuzzer.getCorpusLength();

        assertEquals(previousCorpusLength, updatedCorpusLength);
    }
}
